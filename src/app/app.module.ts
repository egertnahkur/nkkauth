import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Facebook } from '@ionic-native/facebook';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireDatabaseModule } from "angularfire2/database";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
/*import { AuthServiceProvider } from '../providers/auth-service/auth-service';*/
import { FIREBASE_CONFIG } from './app.firebase.config';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { GooglePlus } from "@ionic-native/google-plus";

// imported pages
import { AddFamilyPage } from "../pages/add-family/add-family";
import { AddfamilymemberPage } from "../pages/addfamilymember/addfamilymember";
import { AvalehtPage } from "../pages/avaleht/avaleht";
import { ChimneyswpPage } from "../pages/chimneyswp/chimneyswp";
import { CybersecPage } from "../pages/cybersec/cybersec";
import { ElecnwatoutPage } from "../pages/elecnwatout/elecnwatout";
import { EmergnumsPage } from "../pages/emergnums/emergnums";
import { EvacuationPage } from "../pages/evacuation/evacuation";
import { FiredetectPage } from "../pages/firedetect/firedetect";
import { FireplacesPage } from "../pages/fireplaces/fireplaces";
import { FirepreventPage } from "../pages/fireprevent/fireprevent";
import { FiresafetyPage } from "../pages/firesafety/firesafety";
import { FirstaidPage } from "../pages/firstaid/firstaid";
import { HomesecPage } from "../pages/homesec/homesec";
import { KovgathPage } from "../pages/kovgath/kovgath";
import { LoginPage } from "../pages/login/login";
import { NaiskoduPage } from "../pages/naiskodu/naiskodu";
import { NatrdisastPage } from "../pages/natrdisast/natrdisast";
import { NatrsafetyPage } from "../pages/natrsafety/natrsafety";
import { PasswordresetPage } from "../pages/passwordreset/passwordreset";
import { PerePage } from "../pages/pere/pere";
import { PhysselfdefPage } from "../pages/physselfdef/physselfdef";
import { ProfiilPage } from "../pages/profiil/profiil";
import { ProfilepicPage } from "../pages/profilepic/profilepic";
import { RegisterPage } from "../pages/register/register";
import { RegsuccessPage } from "../pages/regsuccess/regsuccess";
import { SecthreatsPage } from "../pages/secthreats/secthreats";
import { TabsPage } from "../pages/tabs/tabs";
import { VarudPage } from "../pages/varud/varud";
import { VerifyemailPage } from "../pages/verifyemail/verifyemail";
import { ViewallPage } from "../pages/viewall/viewall";
import { WatersafetyPage } from "../pages/watersafety/watersafety";
import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';
import { ImghandlerProvider } from '../providers/imghandler/imghandler';
import { RequestsProvider } from '../providers/requests/requests';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
/*
template for import: 
import {  } from "../pages//";
*/

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '2e3e4d81'
  },
  'auth': {
    'facebook': {
      'scope': []
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    AddFamilyPage,
    AddfamilymemberPage,
    AvalehtPage,
    ChimneyswpPage,
    CybersecPage,
    ElecnwatoutPage,
    EmergnumsPage,
    EvacuationPage,
    FiredetectPage,
    FireplacesPage,
    FirepreventPage,
    FiresafetyPage,
    FirstaidPage,
    HomesecPage,
    KovgathPage,
    LoginPage,
    NaiskoduPage,
    NatrdisastPage,
    NatrsafetyPage,
    PasswordresetPage,
    PerePage,
    PhysselfdefPage,
    ProfiilPage,
    ProfilepicPage,
    RegisterPage,
    RegsuccessPage,
    SecthreatsPage,
    TabsPage,
    VarudPage,
    VerifyemailPage,
    ViewallPage,
    WatersafetyPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {tabsPlacement: 'top'}),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddFamilyPage,
    AddfamilymemberPage,
    AvalehtPage,
    ChimneyswpPage,
    CybersecPage,
    ElecnwatoutPage,
    EmergnumsPage,
    EvacuationPage,
    FiredetectPage,
    FireplacesPage,
    FirepreventPage,
    FiresafetyPage,
    FirstaidPage,
    HomesecPage,
    KovgathPage,
    LoginPage,
    NaiskoduPage,
    NatrdisastPage,
    NatrsafetyPage,
    PasswordresetPage,
    PerePage,
    PhysselfdefPage,
    ProfiilPage,
    ProfilepicPage,
    RegisterPage,
    RegsuccessPage,
    SecthreatsPage,
    TabsPage,
    VarudPage,
    VerifyemailPage,
    ViewallPage,
    WatersafetyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    FilePath,
    FileChooser,
    GooglePlus,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    UserProvider,
    ImghandlerProvider,
    RequestsProvider,
    RequestsProvider,
    LocationTrackerProvider,
    Geolocation,
    BackgroundGeolocation
    /*AuthServiceProvider*/
  ]
})
export class AppModule { }
