import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecthreatsPage } from './secthreats';

@NgModule({
  declarations: [
    SecthreatsPage,
  ],
  imports: [
    IonicPageModule.forChild(SecthreatsPage),
  ],
  exports: [
    SecthreatsPage
  ]
})
export class SecthreatsPageModule {}
