import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VarudPage } from './varud';

@NgModule({
  declarations: [
    VarudPage,
  ],
  imports: [
    IonicPageModule.forChild(VarudPage),
  ],
  exports: [
    VarudPage
  ]
})
export class VarudPageModule {}
