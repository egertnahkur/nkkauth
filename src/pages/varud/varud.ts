import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import * as firebase from 'firebase/app';

//pages

import { VerifyemailPage } from "../verifyemail/verifyemail";
import { LoginPage } from "../login/login";
import { ProfilepicPage } from "../profilepic/profilepic";
import { UserProvider } from '../../providers/user/user';


/**
 * Generated class for the VarudPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-varud',
  templateUrl: 'varud.html',
})
export class VarudPage {

  constructor(private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public userservice: UserProvider) {
  }

  ionViewCanEnter() {
    firebase.auth().onAuthStateChanged((user) => {
      if(!user) {
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  ionViewDidLoad() {
    // kasutaja (edasi: user) staatus (kas on sisse logitud või mitte)
    firebase.auth().onAuthStateChanged((user) => {
      // kui user on sisse logitud
      if (user) {
        // kui user ei ole null; user kindlalt eksisteerib, koos andmetega
        if (user != null) {
          // võtame kasutajalt auth andmebaasist info ja paneme muutujatesse
          var emailVerified = user.emailVerified;
          // initialiseerime ka photoUrl-i, kuna meil läheb seda hiljem nime salvestamisel vaja.
          var photoUrl = user.photoURL;
          console.log("emailverified: ", emailVerified);
          // kui andmebaasis näitab, et user ei ole oma emaili kinnitanud ning on emailiga registreerunud siis saadame ta emaili kinnitama
          if (!emailVerified && firebase.database().ref('/users').child(this.afAuth.auth.currentUser.uid).child("regMethod").key == "email" ) {
            this.navCtrl.setRoot(VerifyemailPage);
            // kui user on emaili kinnitanud, siis: 
          } else {
            /* kui kasutaja ei ole oma profiilipilti sätestanud ning ei ole ise profiilipildi
            /  sätestamist vahele jätnud (nt: kui programm on kogemata kinni läinud/lõpetanud töötamise vms)
            /  , siis saadame ta lehele, kus ta seda teha saab */
            if(!this.userservice.checkImgStatus) {
              this.navCtrl.setRoot(ProfilepicPage);
            }
          }
        }
      } else {
        // kui kasutaja ei ole sisse logitud, siis viime ta sisselogimislehele
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

}
