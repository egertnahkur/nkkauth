import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { applicationUser } from "../../models/interfaces/userCreds";
import { AngularFireAuth } from "angularfire2/auth";
import { FacebookAuth, User } from '@ionic/cloud-angular';

import { RegsuccessPage } from "../regsuccess/regsuccess";
import { ProfilepicPage } from "../profilepic/profilepic";
import { UserProvider } from '../../providers/user/user';



/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  /* user = {} as applicationUser; */
  newuser = {
    email: '',
    password: '',
    displayName: ''
  }

  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public facebookAuth: FacebookAuth, public fbUser: User, public userService: UserProvider,
    public afAuth: AngularFireAuth, private alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    if (this.navParams.get('title') != null) {
      console.log(this.navParams.get('title'));
    } else {
      console.log("none");
    }
  }

  /* async register(user) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      if(user.fullname != null && user.fullname != "") {
        if(result) {
          var curUser = this.afAuth.auth.currentUser;
            curUser.updateProfile({
              displayName: user.fullname,
              photoURL: "https://pbs.twimg.com/profile_images/800034575476924416/aHVU_VqF_400x400.jpg"
            }).then(function() {
              // Update successful.
            }, function(error) {
              // An error happened.
            });
          if(this.navParams.get('title') != null) {
            let title = this.navParams.get('title');
            this.navCtrl.setRoot(RegsuccessPage, {
              title: title
            });
          } else {
            this.navCtrl.setRoot(RegsuccessPage);
          }
        }
      }
    } catch (error) {
      let alert = this.alertCtrl.create({
        title: "Viga registreerimisel",
        subTitle: error,
        buttons: ['OK']
      });
      alert.present();
    }
  } */

  register() {
    let alertErr = this.alertCtrl.create({
      buttons: ['Ok']
    })
    var toaster = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom'
    })
    if (this.newuser.email == '') {
      toaster.setMessage('Sisestage e-meili aadress!');
      toaster.present();
    } else if (this.newuser.displayName == '') {
      toaster.setMessage('Sisestage nimi!');
      toaster.present();
    } else if (this.newuser.password == '') {
      toaster.setMessage('Sisestage parool!');
      toaster.present();
    } else if (this.newuser.password.length < 7) {
      toaster.setMessage('Sisestage tugevam parool! Parool peab olema pikem kui kuus tähemärki!');
      toaster.present();
    } else {
      let loader = this.loadingCtrl.create({
        content: 'Palun oodake...'
      })
      loader.present();
      this.userService.adduser(this.newuser).then((res: any) => {
        loader.dismiss();
        if (res.success)
          this.navCtrl.setRoot(RegsuccessPage);
        else
          alert('Viga registreerimisel: ' + res);
      }).catch((error) => {
        loader.dismiss();
        alertErr.setTitle('Viga');
        alertErr.setSubTitle(error);
        alertErr.present();
      });
    }
  }

}
