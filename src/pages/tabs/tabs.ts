import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import * as firebase from 'firebase/app';

import { AvalehtPage } from "../avaleht/avaleht";
import { PerePage } from "../pere/pere";
import { VarudPage } from "../varud/varud";
import { ProfiilPage } from "../profiil/profiil";


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
@IonicPage()
export class TabsPage {

  user: Observable<firebase.User>;

  tab1 = AvalehtPage
  tab2 = PerePage
  tab3 = VarudPage
  tab4 = ProfiilPage


  constructor(
    private afAuth: AngularFireAuth, public toast: ToastController, private alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {

      this.user = this.afAuth.authState;
  }

  ionViewDidLoad() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.toast.create({
          message: 'Olete hetkel sisse logitud.',
          duration: 3000
        }).present();
        if (user != null) {
          var name = user.displayName;
          var email = user.email;
          var photoUrl = user.photoURL;
          var emailVerified = user.emailVerified;
          var uid = user.uid;
          console.log("name: ", name, ", email: ", email, ", photourl: ", photoUrl, ", emailverified: ", 
          emailVerified, ", uid: ",uid);
          if(!emailVerified) {

          } else {
            if(!(name != null)) {
              let alert = this.alertCtrl.create({
                title: 'Nimi',
                enableBackdropDismiss: false,
                inputs: [
                  {
                    name: 'Nimi',
                    placeholder: 'Sinu ees- ja perekonnanimi.'
                  }
                ],
                buttons: [
                  {
                    text: 'Salvesta',
                    handler: data => {
                      var user = firebase.auth().currentUser;
                      user.updateProfile({
                        displayName: data.Nimi,
                        photoURL: photoUrl
                      }).then(function() {
                        // Update successful.
                      }, function(error) {
                        // An error happened.
                      });
                    }
                  }
                ]
              });
              alert.present();
            }
          }
        }
      } else {
        this.toast.create({
          message: 'Olete hetkel välja logitud.',
          duration: 3000
        }).present();
      }
    });
  }

}
