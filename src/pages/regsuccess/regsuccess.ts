import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { ProfilepicPage } from "../profilepic/profilepic";


/**
 * Generated class for the RegsuccessPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-regsuccess',
  templateUrl: 'regsuccess.html',
})
export class RegsuccessPage {
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  ionViewDidLoad() {
    var user = firebase.auth().currentUser;
    user.sendEmailVerification().then(function() {
      // verifikatsioon saadetud
    }, function(error) {
      // viga kirja saatmisel
      console.error(error);
    });
    firebase.auth().onAuthStateChanged((user) => {
      if(user) {
        var timer = setInterval(() => {
          firebase.auth().currentUser.reload();
          if (firebase.auth().currentUser.emailVerified) {
              this.navCtrl.setRoot(ProfilepicPage);
              console.log("Email Verified!");
              clearInterval(timer);
          }
        }, 1000);
      } else {
        clearInterval(timer);
      }
    })
  }

  sendAgain() {
    var user = firebase.auth().currentUser;
    user.sendEmailVerification().then(function() {
      // verifikatsioon saadetud
    }, function(error) {
      // viga kirja saatmisel
      console.error(error);
    });
  }

}
