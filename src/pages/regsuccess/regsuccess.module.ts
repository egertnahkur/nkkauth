import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegsuccessPage } from './regsuccess';

@NgModule({
  declarations: [
    RegsuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(RegsuccessPage),
  ],
  exports: [
    RegsuccessPage
  ]
})
export class RegsuccessPageModule {}
