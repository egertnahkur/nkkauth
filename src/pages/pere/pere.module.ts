import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerePage } from './pere';

@NgModule({
  declarations: [
    PerePage,
  ],
  imports: [
    IonicPageModule.forChild(PerePage),
  ],
  exports: [
    PerePage
  ]
})
export class PerePageModule {}
