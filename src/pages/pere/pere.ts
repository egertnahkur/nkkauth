import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import { RequestsProvider } from '../../providers/requests/requests';
import * as firebase from 'firebase/app';

//import pages
import { VerifyemailPage } from "../verifyemail/verifyemail";
import { AddfamilymemberPage } from "../addfamilymember/addfamilymember";
import { LoginPage } from "../login/login";
import { ProfilepicPage } from "../profilepic/profilepic";
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the PerePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pere',
  templateUrl: 'pere.html',
})
export class PerePage {
  myrequests;
  myfriends;
  norequests: boolean = true;
  showReqs: boolean = false;
  showFriends: boolean = false;
  user: Observable<firebase.User>;

  constructor(public userservice: UserProvider, private afAuth: AngularFireAuth, public toast: ToastController, private alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams, public events: Events, public requestservice: RequestsProvider) {
    this.user = this.afAuth.authState;
  }

  ionViewWillEnter() {
    this.requestservice.getmyrequests();
    this.requestservice.getmyfriends();
    this.myfriends = [];
    this.events.subscribe('gotrequests', () => {
      this.myrequests = [];
      this.myrequests = this.requestservice.userdetails;
      if(this.myrequests.length > 0) {
        this.showReqs = true;
      } else {
        this.showReqs = false;
      }
    })
    this.events.subscribe('friends', () => {
      this.myfriends = [];
      this.myfriends = this.requestservice.myfriends; 
      if(this.myfriends.length > 0) {
        this.showFriends = true;
      } else {
        this.showFriends = false;
      }
    })
  }

  ionViewDidLeave() {
    this.events.unsubscribe('gotrequests');
    this.events.unsubscribe('friends');
  }

  accept(item) {
    this.requestservice.acceptrequest(item).then(() => {

      let newalert = this.alertCtrl.create({
        title: 'Pereliige lisatud',
        subTitle: 'Klikka pereliikme nimel, et tema profiil avada.',
        buttons: ['Ok']
      });
      newalert.present();
    })
  }

  ignore(item) {
    this.requestservice.deleterequest(item).then(() => {
      alert('Taotlus eiratud.');
    }).catch((err) => {
      alert(err);
    })
  }

  ionViewDidLoad() {
    // kasutaja (edasi: user) staatus (kas on sisse logitud või mitte)
    firebase.auth().onAuthStateChanged((user) => {
      // kui user on sisse logitud
      if (user) {
        // kui user ei ole null; user kindlalt eksisteerib, koos andmetega
        if (user != null) {
          // võtame kasutajalt auth andmebaasist info ja paneme muutujatesse
          var emailVerified = user.emailVerified;
          // initialiseerime ka photoUrl-i, kuna meil läheb seda hiljem nime salvestamisel vaja.
          var photoUrl = user.photoURL;
          console.log("emailverified: ", emailVerified);
          // kui andmebaasis näitab, et user ei ole oma emaili kinnitanud saadame ta siit ära
          if (!emailVerified && firebase.database().ref('/users').child(this.afAuth.auth.currentUser.uid).child("regMethod").key == "email" ) {
            this.navCtrl.setRoot(VerifyemailPage);
            // kui user on emaili kinnitanud, siis: 
          } else {
            /* kui kasutaja ei ole oma profiilipilti sätestanud ning ei ole ise profiilipildi
            /  sätestamist vahele jätnud (nt: kui programm on kogemata kinni läinud/lõpetanud töötamise vms)
            /  , siis saadame ta lehele, kus ta seda teha saab */
            if (!this.userservice.checkImgStatus) {
              this.navCtrl.setRoot(ProfilepicPage);
            }
          }
        }
      } else {
        // kui kasutaja ei ole sisse logitud, siis viime ta sisselogimislehele
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  addFamily() {
    this.navCtrl.push(AddfamilymemberPage);
  }
}
