import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NatrsafetyPage } from './natrsafety';

@NgModule({
  declarations: [
    NatrsafetyPage,
  ],
  imports: [
    IonicPageModule.forChild(NatrsafetyPage),
  ],
  exports: [
    NatrsafetyPage
  ]
})
export class NatrsafetyPageModule {}
