import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WatersafetyPage } from './watersafety';

@NgModule({
  declarations: [
    WatersafetyPage,
  ],
  imports: [
    IonicPageModule.forChild(WatersafetyPage),
  ],
  exports: [
    WatersafetyPage
  ]
})
export class WatersafetyPageModule {}
