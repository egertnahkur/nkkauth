import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewallPage } from './viewall';

@NgModule({
  declarations: [
    ViewallPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewallPage),
  ],
  exports: [
    ViewallPage
  ]
})
export class ViewallPageModule {}
