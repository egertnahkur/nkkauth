import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


//import pages
import { CybersecPage } from "../cybersec/cybersec";
import { ElecnwatoutPage } from "../elecnwatout/elecnwatout";
import { FiresafetyPage } from "../firesafety/firesafety";
import { FirstaidPage } from "../firstaid/firstaid";
import { HomesecPage } from "../homesec/homesec";
import { NatrdisastPage } from "../natrdisast/natrdisast";
import { NatrsafetyPage } from "../natrsafety/natrsafety";
import { PhysselfdefPage } from "../physselfdef/physselfdef";
import { SecthreatsPage } from "../secthreats/secthreats";
import { WatersafetyPage } from "../watersafety/watersafety";

/**
 * Generated class for the ViewallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-viewall',
  templateUrl: 'viewall.html',
})
export class ViewallPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewallPage');
  }

  vCSec() {
  	this.navCtrl.push(CybersecPage);
  }

  vEWOut() {
  	this.navCtrl.push(ElecnwatoutPage);
  }

  vFSaf() {
  	this.navCtrl.push(FiresafetyPage);
  }

  vFAid() {
  	this.navCtrl.push(FirstaidPage);
  }

  vHSec() {
  	this.navCtrl.push(HomesecPage);
  }

  vNDis() {
  	this.navCtrl.push(NatrdisastPage);
  }

  vNSaf() {
  	this.navCtrl.push(NatrsafetyPage);
  }

  vPDef() {
  	this.navCtrl.push(PhysselfdefPage);
  }

  vSThrt() {
  	this.navCtrl.push(SecthreatsPage);
  }

  vWSaf() {
  	this.navCtrl.push(WatersafetyPage);
  }

}
