import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiresafetyPage } from './firesafety';

@NgModule({
  declarations: [
    FiresafetyPage,
  ],
  imports: [
    IonicPageModule.forChild(FiresafetyPage),
  ],
  exports: [
    FiresafetyPage
  ]
})
export class FiresafetyPageModule {}
