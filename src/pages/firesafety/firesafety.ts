import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import pages
import { FireplacesPage } from "../fireplaces/fireplaces";
import { FirepreventPage } from "../fireprevent/fireprevent";
import { FiredetectPage } from "../firedetect/firedetect";
import { EvacuationPage } from "../evacuation/evacuation";
import { ChimneyswpPage } from "../chimneyswp/chimneyswp";

/**
 * Generated class for the FiresafetyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-firesafety',
  templateUrl: 'firesafety.html',
})
export class FiresafetyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiresafetyPage');
  }

  vFireplaces() {
  	this.navCtrl.push(FireplacesPage);
  }
  vFireprevent() {
  	this.navCtrl.push(FirepreventPage);
  }
  vFiredetect() {
  	this.navCtrl.push(FiredetectPage);
  }
  vEvacuation() {
  	this.navCtrl.push(EvacuationPage);
  }
  vChimneyswp() {
  	this.navCtrl.push(ChimneyswpPage);
  }

}
