import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the PasswordresetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-passwordreset',
  templateUrl: 'passwordreset.html',
})
export class PasswordresetPage {
  email: string;
  constructor(public userService: UserProvider, public alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  forgotPass() {
    let alert = this.alertCtrl.create({
      buttons: ['Ok']
    })
    this.userService.passwordReset(this.email).then((res: any) => {
      if(res.success)  {
          alert.setTitle('E-meil saadetud');
          alert.setSubTitle('Parooli taastamislink saadetud aadressile ' + this.email);
          alert.present();
        }
      else {
        alert.setTitle('Viga');
        alert.setSubTitle('Viga parooli taastamislingi saatmisel');
        alert.present();
      }
    }).catch((err) => {
      alert.setTitle('Viga');
      alert.setSubTitle(err);
      alert.present();
    })
  }

  ionViewDidLoad() {
  }

}
