import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CybersecPage } from './cybersec';

@NgModule({
  declarations: [
    CybersecPage,
  ],
  imports: [
    IonicPageModule.forChild(CybersecPage),
  ],
  exports: [
    CybersecPage
  ]
})
export class CybersecPageModule {}
