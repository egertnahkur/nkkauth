import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NatrdisastPage } from './natrdisast';

@NgModule({
  declarations: [
    NatrdisastPage,
  ],
  imports: [
    IonicPageModule.forChild(NatrdisastPage),
  ],
  exports: [
    NatrdisastPage
  ]
})
export class NatrdisastPageModule {}
