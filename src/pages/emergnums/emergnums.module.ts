import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmergnumsPage } from './emergnums';

@NgModule({
  declarations: [
    EmergnumsPage,
  ],
  imports: [
    IonicPageModule.forChild(EmergnumsPage),
  ],
  exports: [
    EmergnumsPage
  ]
})
export class EmergnumsPageModule {}
