import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from "rxjs/Observable";
import * as firebase from 'firebase/app';

import { ViewallPage } from "../viewall/viewall";
import { CybersecPage } from "../cybersec/cybersec";
import { ElecnwatoutPage } from "../elecnwatout/elecnwatout";
import { FiresafetyPage } from "../firesafety/firesafety";
import { FirstaidPage } from "../firstaid/firstaid";
import { HomesecPage } from "../homesec/homesec";
import { NatrdisastPage } from "../natrdisast/natrdisast";
import { NatrsafetyPage } from "../natrsafety/natrsafety";
import { PhysselfdefPage } from "../physselfdef/physselfdef";
import { SecthreatsPage } from "../secthreats/secthreats";
import { WatersafetyPage } from "../watersafety/watersafety";
import { EmergnumsPage } from "../emergnums/emergnums";
import { KovgathPage } from "../kovgath/kovgath";
import { NaiskoduPage } from "../naiskodu/naiskodu";
import { LoginPage } from "../login/login";


// facebook authentication: 
// https://firebase.google.com/docs/auth/web/facebook-login

/**
 * Generated class for the AvalehtPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-avaleht',
  templateUrl: 'avaleht.html',
})
export class AvalehtPage {
  logged = false;
  user: Observable<firebase.User>;

  constructor(
    private afAuth: AngularFireAuth, public toast: ToastController, private alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {

    this.user = this.afAuth.authState;
  }

  /*ionViewDidLoad() {
    this.afAuth.authState.subscribe(data => {
      if(this.afAuth && data && data.email && data.uid) {
        this.logged = true;
        console.log(this.logged);
        this.toast.create({
          message: 'Olete hetkel sisse logitud.',
          duration: 3000
        }).present();
      } else {
        this.toast.create({
          message: 'Olete hetkel välja logitud.',
          duration: 3000
        }).present();
      }
    });
    console.log('ionViewDidLoad AvalehtPage');
  }*/

  async awaitUser() {
    this.user = await this.afAuth.authState;
  }

  viewAll() {
    this.navCtrl.push(ViewallPage);
  }

  vCSec() {
    this.navCtrl.push(CybersecPage);
  }

  vEWOut() {
    this.navCtrl.push(ElecnwatoutPage);
  }

  vFSaf() {
    this.navCtrl.push(FiresafetyPage);
  }

  vFAid() {
    this.navCtrl.push(FirstaidPage);
  }

  vHSec() {
    this.navCtrl.push(HomesecPage);
  }

  vNDis() {
    this.navCtrl.push(NatrdisastPage);
  }

  vNSaf() {
    this.navCtrl.push(NatrsafetyPage);
  }

  vPDef() {
    this.navCtrl.push(PhysselfdefPage);
  }

  vSThrt() {
    this.navCtrl.push(SecthreatsPage);
  }

  vWSaf() {
    this.navCtrl.push(WatersafetyPage);
  }

  vEmerg() {
    this.navCtrl.push(EmergnumsPage);
  }

  vKOVGath() {
    this.navCtrl.push(KovgathPage);
  }

  vNKKaitse() {
    this.navCtrl.push(NaiskoduPage);
  }

  logIn() {
    this.navCtrl.push(LoginPage);
  }
  checkLogin() {
    if (this.logged == true) {
      return true;
    } else {
      return false;
    }
  }

  /*async logOut() {
    try {
      const result = await this.afAuth.auth.signOut;
      if(result) {
        localStorage.clear();
        location.reload();
        this.toast.create({
          message: 'Olete välja logitud.',
          duration: 3000
        }).present();
      }
    } catch (error) {
      console.error(error);
    }
  }*/

  refresh() {
    location.reload();
  }

  logOut() {
    firebase.auth().signOut().then(() => {
      //this.navCtrl.setRoot(LoginPage);
      
    }).catch(function (error) {
      var errorMsg = '';
      errorMsg = error.message;
      let alert = this.alertCtrl.create({
        title: "Viga väljalogimisel",
        subTitle: errorMsg,
        buttons: ['OK']
      });
      alert.present();
    });
  }

  /*logOut() {
    firebase.auth().signOut().then(function() {
      
    }).catch((error) => {
      
    });
  }*/
}
