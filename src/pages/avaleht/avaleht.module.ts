import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvalehtPage } from './avaleht';

@NgModule({
  declarations: [
    AvalehtPage,
  ],
  imports: [
    IonicPageModule.forChild(AvalehtPage),
  ],
  exports: [
    AvalehtPage
  ]
})
export class AvalehtPageModule {}
