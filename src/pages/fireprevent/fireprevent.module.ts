import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirepreventPage } from './fireprevent';

@NgModule({
  declarations: [
    FirepreventPage,
  ],
  imports: [
    IonicPageModule.forChild(FirepreventPage),
  ],
  exports: [
    FirepreventPage
  ]
})
export class FirepreventPageModule {}
