import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FireplacesPage } from './fireplaces';

@NgModule({
  declarations: [
    FireplacesPage,
  ],
  imports: [
    IonicPageModule.forChild(FireplacesPage),
  ],
  exports: [
    FireplacesPage
  ]
})
export class FireplacesPageModule {}
