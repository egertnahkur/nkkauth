import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiredetectPage } from './firedetect';

@NgModule({
  declarations: [
    FiredetectPage,
  ],
  imports: [
    IonicPageModule.forChild(FiredetectPage),
  ],
  exports: [
    FiredetectPage
  ]
})
export class FiredetectPageModule {}
