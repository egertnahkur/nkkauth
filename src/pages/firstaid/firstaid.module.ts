import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirstaidPage } from './firstaid';

@NgModule({
  declarations: [
    FirstaidPage,
  ],
  imports: [
    IonicPageModule.forChild(FirstaidPage),
  ],
  exports: [
    FirstaidPage
  ]
})
export class FirstaidPageModule {}
