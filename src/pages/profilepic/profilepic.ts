import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ImghandlerProvider } from '../../providers/imghandler/imghandler';
import { UserProvider } from '../../providers/user/user';

import { RegsuccessPage } from "../regsuccess/regsuccess";
import { AvalehtPage } from "../avaleht/avaleht";

import { AngularFireAuth } from "angularfire2/auth";

import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-profilepic',
  templateUrl: 'profilepic.html',
})
export class ProfilepicPage {
  firedata = firebase.database().ref('/users');
  imgurl = this.afAuth.auth.currentUser.photoURL;
  moveon = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public imgservice: ImghandlerProvider,
    public zone: NgZone, public userservice: UserProvider, public loadingCtrl: LoadingController, public afAuth: AngularFireAuth) {
  }

  ionViewDidLoad() {
    this.getCurImg();
  }

  async getCurImg() {
    this.imgurl = await this.afAuth.auth.currentUser.photoURL;
    return this.imgurl;
  }

  /* chooseimage() {
    let loader = this.loadingCtrl.create({
      content: 'Palun oodake...'
    })
    loader.present();
    this.imgservice.uploadimage().then((uploadedurl: any) => {
      loader.dismiss();
      this.zone.run(() => {
        this.imgurl = uploadedurl;
        this.moveon = false;
      })
    })
  } */

  chooseimage() {
    /* let loader = this.loadingCtrl.create({
      content: 'Palun oodake...'
    })
    loader.present(); */
    this.imgservice.uploadimage().then((uploadedurl: any) => {
      /* loader.dismiss(); */
      this.zone.run(() => {
        this.imgurl = uploadedurl;
        this.moveon = false;
      })
    }).catch((res) => {
      alert(res);
    })
  }

  /* updateproceed() {
    let loader = this.loadingCtrl.create({
      content: 'Palun oodake...'
    })
    loader.present();
    this.userservice.updateimage(this.imgurl).then((res: any) => {
      loader.dismiss();
      if (res.success) {
        this.navCtrl.setRoot(AvalehtPage);
      }
      else {
        alert(res);
      }
    })
  } */

  updateproceed() {
    let loader = this.loadingCtrl.create({
      content: 'Palun oodake...'
    })
    loader.present();
    this.userservice.updateimage(this.imgurl).then((res: any) => {
      loader.dismiss();
      if (res.success) {
        this.navCtrl.setRoot(AvalehtPage);
      }
      else {
        alert(res);
      }
    }).catch((err) => {
      alert(err);
    })
  }

  proceed() {
    this.firedata.child(this.afAuth.auth.currentUser.uid).child('skippedImg').set(true);
    this.navCtrl.setRoot(AvalehtPage);

  }

}