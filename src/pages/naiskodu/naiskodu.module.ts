import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NaiskoduPage } from './naiskodu';

@NgModule({
  declarations: [
    NaiskoduPage,
  ],
  imports: [
    IonicPageModule.forChild(NaiskoduPage),
  ],
  exports: [
    NaiskoduPage
  ]
})
export class NaiskoduPageModule {}
