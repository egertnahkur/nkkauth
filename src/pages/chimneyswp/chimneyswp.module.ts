import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChimneyswpPage } from './chimneyswp';

@NgModule({
  declarations: [
    ChimneyswpPage,
  ],
  imports: [
    IonicPageModule.forChild(ChimneyswpPage),
  ],
  exports: [
    ChimneyswpPage
  ]
})
export class ChimneyswpPageModule {}
