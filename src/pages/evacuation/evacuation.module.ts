import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EvacuationPage } from './evacuation';

@NgModule({
  declarations: [
    EvacuationPage,
  ],
  imports: [
    IonicPageModule.forChild(EvacuationPage),
  ],
  exports: [
    EvacuationPage
  ]
})
export class EvacuationPageModule {}
