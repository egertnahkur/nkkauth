import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhysselfdefPage } from './physselfdef';

@NgModule({
  declarations: [
    PhysselfdefPage,
  ],
  imports: [
    IonicPageModule.forChild(PhysselfdefPage),
  ],
  exports: [
    PhysselfdefPage
  ]
})
export class PhysselfdefPageModule {}
