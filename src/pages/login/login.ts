import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { applicationUser } from "../../models/interfaces/userCreds";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import { FacebookAuth, User, AuthLoginResult } from '@ionic/cloud-angular';
import { GooglePlus } from "@ionic-native/google-plus";

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';

//import pages
import { RegisterPage } from "../register/register";
import { TabsPage } from "../tabs/tabs";
import { AvalehtPage } from "../avaleht/avaleht";
import { RegsuccessPage } from "../regsuccess/regsuccess";
import { PasswordresetPage } from "../passwordreset/passwordreset";
import { ProfilepicPage } from "../profilepic/profilepic";
/*import { TabsPage } from "../tabs/tabs";*/

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  facebookLoginDetails: AuthLoginResult;
  firedata = firebase.database().ref('/users');
  user = {} as applicationUser;

  constructor(public authService: AuthProvider, public userservice: UserProvider,
    public facebookAuth: FacebookAuth, public fbUser: User, public googlePlus: GooglePlus,
    public afAuth: AngularFireAuth, private alertCtrl: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
  }
  register() {
    if (this.navParams.get('title') != null) {
      let title = this.navParams.get('title');
      this.navCtrl.push(RegisterPage, {
        title: title
      });
    } else {
      this.navCtrl.push(RegisterPage);
    }
  }
  /* forgotPass() {
    let alert = this.alertCtrl.create({
      title: 'Taasta parool',
      inputs: [
        {
          name: 'email',
          placeholder: 'Sisesta e-posti aadress.',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Tühista',
          role: 'cancel',
          handler: data => {
            console.log("tühistati");
          }
        },
        {
          text: 'Saada link',
          handler: data => {
            var auth = firebase.auth();
            auth.sendPasswordResetEmail(data.email).then(function () {
              // Email sent.
            }, function (error) {
              { { error } }
            });
          }
        }
      ]
    });
    // toome hüpikakna esile
    alert.present();
  } */

  forgotPass() {
    this.navCtrl.push(PasswordresetPage);
  }

  /* googlePlusRegister() {
    this.googlePlus.login({})
      .then((res) => {
        alert(JSON.stringify(res));
        this.navCtrl.setRoot(RegsuccessPage);
      }).catch((err) => {
        alert(JSON.stringify(err));
      });
  } */

  googlePlusRegister() {
    
    let alertErr = this.alertCtrl.create({
      buttons: ['Ok']
    })
    var provider = new firebase.auth.GoogleAuthProvider();
    provider.setCustomParameters({
      'display': 'popup'
    })

    firebase.auth().signInWithPopup(provider).then((result) => {
      var curuser = this.afAuth.auth.currentUser;
      this.firedata.once('value', function (snapshot) {

        if (snapshot.hasChild(curuser.uid)) {

        } else {
          firebase.database().ref('/users').child(curuser.uid).set({
            uid: curuser.uid,
            displayName: curuser.displayName,
            email: curuser.email,
            photoURL: curuser.photoURL,
            skippedImg: false,
            regMethod: "google"
          })
        }
      }).then(() => {
        var imgstatus: boolean;
        this.userservice.getuserdetails().then((res: any) => {
          imgstatus = res.skippedImg;
        }).then(() => {
          if(!imgstatus) {
            this.navCtrl.setRoot(ProfilepicPage);
          } else if(imgstatus) {
            this.navCtrl.setRoot(AvalehtPage);
          }
        })
      });

    }).catch(function (error) {
      var errorMessage = error.message;
      console.log(errorMessage);
      alertErr.setTitle('Viga');
      alertErr.setSubTitle(errorMessage);
      alertErr.present();
    })

  }

  /* fbRegister() {
    let that = this;
    let provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().signInWithRedirect(provider).then(()=> {
      firebase.auth().getRedirectResult().then((result)=>{
        alert(JSON.stringify(result));
        that.navCtrl.setRoot(RegsuccessPage);
      }).catch(function(error) {
        alert(JSON.stringify(error));
      });
    });
  }

  fbRegisterNew() {
    let that = this;
    let provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().signInWithRedirect(provider).then(()=> {
      firebase.auth().getRedirectResult().then((result)=>{
        this.fbRegisterAddToDatabase();
        alert(JSON.stringify(result));
        that.navCtrl.setRoot(RegsuccessPage);
      }).catch(function(error) {
        alert(JSON.stringify(error));
      });
    });
  }

  fbRegisterAddToDatabase() {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.afAuth.auth.currentUser.uid).set({
        uid: this.afAuth.auth.currentUser.uid,
        displayName: this.afAuth.auth.currentUser.displayName,
        email: this.afAuth.auth.currentUser.email,
        photoURL: 'https://pbs.twimg.com/profile_images/800034575476924416/aHVU_VqF_400x400.jpg',
        skippedImg: false,
        regMethod: "fb"
      }).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err)
      })
    })
    return promise;
  }

   */



  fbRegisterNew() {
    let alertErr = this.alertCtrl.create({
      buttons: ['Ok']
    })
    var provider = new firebase.auth.FacebookAuthProvider();
    provider.setCustomParameters({
      'display': 'popup'
    })

    firebase.auth().signInWithPopup(provider).then((result) => {
      var curuser = this.afAuth.auth.currentUser;
      this.firedata.once('value', function (snapshot) {

        if (snapshot.hasChild(curuser.uid)) {
          
        } else {
          firebase.database().ref('/users').child(curuser.uid).set({
            uid: curuser.uid,
            displayName: curuser.displayName,
            email: curuser.email,
            photoURL: curuser.photoURL,
            skippedImg: false,
            regMethod: "facebook"
          })
        }
      }).then(() => {
        var imgstatus: boolean;
        this.userservice.getuserdetails().then((res: any) => {
          imgstatus = res.skippedImg;
        }).then(() => {
          if(!imgstatus) {
            this.navCtrl.setRoot(ProfilepicPage);
          } else if(imgstatus) {
            this.navCtrl.setRoot(AvalehtPage);
          }
        })
      });

    }).catch(function (error) {
      var errorMessage = error.message;
      console.log(errorMessage);
      alertErr.setTitle('Viga');
      alertErr.setSubTitle(errorMessage);
      alertErr.present();
    })

  }

  async login(user: applicationUser) {
    try {
      const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if (result) {
        location.reload();
        //this.navCtrl.setRoot(TabsPage);
      }
    } catch (error) {
      let alert = this.alertCtrl.create({
        title: "Viga sisselogimisel",
        subTitle: error,
        buttons: ['OK']
      });
      alert.present();
    }
  }

  /* login() {
    this.authService.login(this.user).then((res: any) => {
      if(!res.code) 
        this.navCtrl.setRoot(TabsPage);
      else 
        alert(res);
    });
  } */

}
