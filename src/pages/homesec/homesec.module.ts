import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomesecPage } from './homesec';

@NgModule({
  declarations: [
    HomesecPage,
  ],
  imports: [
    IonicPageModule.forChild(HomesecPage),
  ],
  exports: [
    HomesecPage
  ]
})
export class HomesecPageModule {}
