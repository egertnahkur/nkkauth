import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfiilPage } from './profiil';

@NgModule({
  declarations: [
    ProfiilPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfiilPage),
  ],
  exports: [
    ProfiilPage
  ]
})
export class ProfiilPageModule {}
