import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

import { VerifyemailPage } from "../verifyemail/verifyemail";
import { LoginPage } from "../login/login";
import { ProfilepicPage } from "../profilepic/profilepic";
import { UserProvider } from '../../providers/user/user';

import { ImghandlerProvider } from '../../providers/imghandler/imghandler';

import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';


/**
 * Generated class for the ProfiilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profiil',
  templateUrl: 'profiil.html',
})
export class ProfiilPage {
  curUsrName: any;
  curUsrEmail: any;
  curUsrPic: any;
  curUsrID: any;
  curUsrToken: any;
  toggleValue: boolean = false;
  constructor(public locationTracker: LocationTrackerProvider, public loadCtrl: LoadingController, public zone: NgZone, public imghandler: ImghandlerProvider,
    private afAuth: AngularFireAuth, public toast: ToastController, public navCtrl: NavController,
    public navParams: NavParams, private alertCtrl: AlertController, public userservice: UserProvider) {
  }

  ionViewWillEnter() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user && user != null) {
        this.loaduserdetails();
      }
    })
  }

  geolocationbtn() {
    if(this.toggleValue) {
      this.locationTracker.startTracking();
    } else {
      this.locationTracker.stopTracking();
    }
  }

  starttracking(){
    this.locationTracker.startTracking();
  }
 
  stoptracking(){
    this.locationTracker.stopTracking();
  }

  loaduserdetails() {
    let loader = this.loadCtrl.create({
      content: 'Palun oodake...'
    })
    loader.present();
    this.userservice.getuserdetails().then((res: any) => {
      loader.dismiss();
      this.curUsrName = res.displayName;
      this.curUsrEmail = res.email;
      this.zone.run(() => {
        this.curUsrPic = res.photoURL;
      })
    })
  }

  logout() {
    firebase.auth().signOut().then(() => {
      this.navCtrl.parent.parent.setRoot(LoginPage);
    })
  }

  ionViewCanEnter() {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user || user == null) {
        console.log("ionviewcanenter: no user logged in");
        this.navCtrl.setRoot(LoginPage, {
          title: "ProfiilPage"
        });
      }
    });

  }

  ionViewDidLoad() {
  }

  changeName() {
    let statusalert = this.alertCtrl.create({
      buttons: ['Ok']
    });
    let alert = this.alertCtrl.create({
      title: 'Muuda nime',
      inputs: [{
        name: 'nickname',
        placeholder: 'Täisnimi'
      }],
      buttons: [{
        text: 'Tühista',
        role: 'cancel',
        handler: data => {

        }
      },
      {
        text: 'Uuenda',
        handler: data => {
          if (data.nickname) {
            this.userservice.updatedisplayname(data.nickname).then((res: any) => {
              if (res.success) {
                statusalert.setTitle('Uuendatud');
                statusalert.setSubTitle('Nimi edukalt uuendatud!');
                statusalert.present();
                this.zone.run(() => {
                  this.curUsrName = data.nickname;
                })
              }

              else {
                statusalert.setTitle('Viga');
                statusalert.setSubTitle('Nime uuendamisel tekkis viga, palun kontrolli interneti sätteid või taaskäivita äpp ning proovi uuesti');
                statusalert.present();
              }

            })
          }
        }

      }]
    });
    alert.present();
  }

  /* changePicture() {
    let that = this;
    let alert = this.alertCtrl.create({
      title: 'Pilt',
      inputs: [
        {
          name: 'pic',
          placeholder: 'Sisesta pildi URL.'
        }
      ],
      buttons: [
        {
          text: 'Tühista',
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Salvesta',
          handler: data => {
            var user = firebase.auth().currentUser;
            user.updateProfile({
              displayName: user.displayName,
              photoURL: data.pic
            }).then(function () {
              that.toast.create({
                message: 'Pilt edukalt uuendatud. Tõmba alla, et profiili uuendada.',
                duration: 3000
              }).present();
            }, function (error) {
              that.toast.create({
                message: 'Viga pildi vahetamisel: ' + error,
                duration: 8000
              }).present();
            });
          }
        }
      ]
    });
    alert.present();
  } */

  /* changePicture() {
    this.navCtrl.push(ProfilepicPage);
  } */

  changePicture() {
    let statusalert = this.alertCtrl.create({
      buttons: ['okay']
    });
    this.imghandler.uploadimage().then((url: any) => {
      this.userservice.updateimage(url).then((res: any) => {
        if (res.success) {
          statusalert.setTitle('Uuendatud');
          statusalert.setSubTitle('Teie profiilipilt on uuendatud! Kui te ei näe oma profiilil uut pilti, siis tõmmake ekraanil näpuga ülalt alla, et profiili käsitsi värskendada.');
          statusalert.present();
          this.zone.run(() => {
            this.curUsrPic = url;
          })
        }
      }).catch((err) => {
        statusalert.setTitle('Viga uuendamisel');
        statusalert.setSubTitle('Teie profiilipildi uuendamisel tekkis viga, palun kontrollige oma internetiseadeid ja/või tehke äppile taaskäivitus.');
        statusalert.present();
      })
    })
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    var user = firebase.auth().currentUser;
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
      this.curUsrName = user.displayName;
      this.curUsrEmail = user.email;
      this.curUsrPic = user.photoURL;
    }, 1500);
  }

  changeEmail() {
    let that = this;
    let alert = this.alertCtrl.create({
      title: 'E-mail',
      inputs: [
        {
          name: 'Email',
          placeholder: 'Teie e-meili aadress',
          type: "email"
        }
      ],
      buttons: [
        {
          text: 'Tühista',
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Salvesta',
          handler: data => {
            var user = firebase.auth().currentUser;
            user.updateEmail(data.Email).then(() => {
              that.toast.create({
                message: 'E-maili aadress uuendatud.',
                duration: 3000
              }).present();
            }, function (error) {
              that.toast.create({
                message: 'E-maili aadressi uuendamisel tekkis viga: ' + error,
                duration: 8000
              }).present();
            });
          }
        }
      ]
    });
    alert.present();
  }

  changePassword() {
    let that = this;
    let alert = this.alertCtrl.create({
      title: 'Parool',
      subTitle: 'Sisesta uus parool',
      inputs: [
        {
          name: 'password',
          placeholder: 'Uus parool',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Tühista',
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Salvesta',
          handler: data => {
            var user = firebase.auth().currentUser;
            user.updatePassword(data.password).then(() => {
              that.toast.create({
                message: 'Parool edukalt uuendatud.',
                duration: 3000
              }).present();
            }, function (error) {
              that.toast.create({
                message: 'Parooli uuendamisel tekkis tõrge: ' + error,
                duration: 8000
              }).present();
            });
          }
        }
      ]
    });
    alert.present();
  }
}
