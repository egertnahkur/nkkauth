import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KovgathPage } from './kovgath';

@NgModule({
  declarations: [
    KovgathPage,
  ],
  imports: [
    IonicPageModule.forChild(KovgathPage),
  ],
  exports: [
    KovgathPage
  ]
})
export class KovgathPageModule {}
