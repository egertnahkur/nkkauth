import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElecnwatoutPage } from './elecnwatout';

@NgModule({
  declarations: [
    ElecnwatoutPage,
  ],
  imports: [
    IonicPageModule.forChild(ElecnwatoutPage),
  ],
  exports: [
    ElecnwatoutPage
  ]
})
export class ElecnwatoutPageModule {}
