import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { ProfiilPage } from "../profiil/profiil";
import { VarudPage } from "../varud/varud";
import { PerePage } from "../pere/pere";
import { AvalehtPage } from "../avaleht/avaleht";

/**
 * Generated class for the VerifyemailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-verifyemail',
  templateUrl: 'verifyemail.html',
})
export class VerifyemailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    firebase.auth().onAuthStateChanged((user) => {
      if(user) {
        var timer = setInterval(() => {
          firebase.auth().currentUser.reload();
          if (firebase.auth().currentUser.emailVerified) {
            if(this.navParams.get('title') != null) {
              let title = this.navParams.get('title');
              this.navCtrl.setRoot(title)
              console.log("Email Verified!");
              clearInterval(timer);
            } else {
              this.navCtrl.setRoot(AvalehtPage);
              console.log("Email Verified!");
              clearInterval(timer);
            }
          }
        }, 1000);
      } else {
        clearInterval(timer);
      }
    })
  }

  sendAgain() {
    var user = firebase.auth().currentUser;
    user.sendEmailVerification().then(function () {
      // verifikatsioon saadetud
    }, function (error) {
      // viga kirja saatmisel
      console.error(error);
    });
  }

}
