import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyemailPage } from './verifyemail';

@NgModule({
  declarations: [
    VerifyemailPage,
  ],
  imports: [
    IonicPageModule.forChild(VerifyemailPage),
  ],
  exports: [
    VerifyemailPage
  ]
})
export class VerifyemailPageModule {}
