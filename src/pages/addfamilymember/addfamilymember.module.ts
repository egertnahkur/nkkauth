import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddfamilymemberPage } from './addfamilymember';

@NgModule({
  declarations: [
    AddfamilymemberPage,
  ],
  imports: [
    IonicPageModule.forChild(AddfamilymemberPage),
  ],
  exports: [
    AddfamilymemberPage
  ]
})
export class AddfamilymemberPageModule {}
