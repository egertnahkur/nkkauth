import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { RequestsProvider } from '../../providers/requests/requests';
import { connreq } from '../../models/interfaces/request';
import { AngularFireAuth } from "angularfire2/auth";
import firebase from 'firebase';

/**
 * Generated class for the AddfamilymemberPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-addfamilymember',
  templateUrl: 'addfamilymember.html',
})
export class AddfamilymemberPage {
  newrequest = {} as connreq;
  temparr = [];
  filteredusers = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth,
    public userservice: UserProvider, public alertCtrl: AlertController,
    public requestservice: RequestsProvider) {
    this.userservice.displayfilteredusers().then((res: any) => {
      this.filteredusers = res;
      this.temparr = res;
    })
  }

  ionViewDidLoad() {

  }

  searchuser(searchbar) {
    this.filteredusers = this.temparr;
    var q = searchbar.target.value;
    if (q.trim() == '') {
      return;
    }

    this.filteredusers = this.filteredusers.filter((v) => {
      if (v.displayName.toLowerCase().indexOf(q.toLowerCase()) > -1 || v.email.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      return false;
    })
  }

  sendreq(recipient) {
    this.newrequest.sender = firebase.auth().currentUser.uid;
    this.newrequest.recipient = recipient.uid;
    if (this.newrequest.sender === this.newrequest.recipient)
      alert('Ennast ei saa pereliikmeks lisada :)');
    else {
      let successalert = this.alertCtrl.create({
        title: 'Kutse saadetud',
        subTitle: 'Sinu kutse saadeti kasutajale ' + recipient.displayName,
        buttons: ['Ok']
      });

      this.requestservice.sendrequest(this.newrequest).then((res: any) => {
        if (res.success) {
          successalert.present();
          let sentuser = this.filteredusers.indexOf(recipient);
          this.filteredusers.splice(sentuser, 1);
          /* firebase.database().ref('/users/').child(this.afAuth.auth.currentUser.uid).child('requests').child(recipient.uid).push({
            requestType: "sent"
          }); */
        }
      }).catch((err) => {
        alert(err);
      })
    }
  }

}
