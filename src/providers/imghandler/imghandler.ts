import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { LoadingController } from 'ionic-angular';
import { FilePath } from '@ionic-native/file-path';
import { Camera, CameraOptions } from '@ionic-native/camera';
import firebase from 'firebase';

@Injectable()
export class ImghandlerProvider {
  nativepath: any;
  firestore = firebase.storage();
  constructor(public fileChooser: FileChooser, public loadingCtrl: LoadingController, public camera: Camera) {
  }

  uploadimage() {
    let loader = this.loadingCtrl.create({
      content: 'Palun oodake...'
    });
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    var promise = new Promise((resolve, reject) => {
      //alert("1");

      this.camera.getPicture(options).then((imageData) => {
        loader.present();
        var imageStore = this.firestore.ref('/profileimages').child(firebase.auth().currentUser.uid);
        imageStore.putString(imageData, 'base64').then((res) => {
          this.firestore.ref('/profileimages').child(firebase.auth().currentUser.uid).getDownloadURL().then((url) => {
            loader.dismiss()
            resolve(url);
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })

      }, (err) => {
      }).catch((err) => {
        reject(err);
      });
      loader.dismiss();
    })
    return promise;
  }

}