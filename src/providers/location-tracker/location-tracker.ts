import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/filter';

@Injectable()
export class LocationTrackerProvider {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;
  firedata = firebase.database().ref('/users');
  constructor(public zone: NgZone, public backgroundGeolocation: BackgroundGeolocation, public geolocation: Geolocation) {

  }

  startTracking() {

    // Background Tracking

    let config = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      debug: true,
      interval: 2000
    };

    this.backgroundGeolocation.configure(config).subscribe((location) => {

      console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = location.latitude;
        this.lng = location.longitude;
        var that = this;
        this.firedata.update('value', function (snapshot) {
          firebase.database().ref('/users').child(firebase.auth().currentUser.uid).child('/geolocation').update({
            latitude: that.lat,
            longitude: that.lng
          })
        })

        /* firebase.database().ref('/geolocation/').child(firebase.auth().currentUser.uid).once('value', function (snapshot) {
          this.firedata.set({
            latitude: this.lat,
            longitude: this.lng
          })
        }) */
      });

    }, (err) => {

      console.log(err);

    });

    // Turn ON the background-geolocation system.
    this.backgroundGeolocation.start();


    // Foreground Tracking

    let options = {
      frequency: 3000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {

      console.log(position);

      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        var that = this;
        this.firedata.once('value', function (snapshot) {
          firebase.database().ref('/users').child(firebase.auth().currentUser.uid).child('/geolocation').update({
            latitude: that.lat,
            longitude: that.lng
          })
        })
      });

    });

  }

  stopTracking() {

    console.log('stopTracking');

    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();

  }

}