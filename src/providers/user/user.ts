import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormControl } from '@angular/forms';
import firebase from 'firebase';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserProvider {
  firedata = firebase.database().ref('/users');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  imgStatus: boolean;
  constructor(public afAuth: AngularFireAuth, public afireauth: AngularFireAuth) {
  }

  adduser(newuser) {
    var promise = new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(() => {
        this.afAuth.auth.currentUser.updateProfile({
          displayName: newuser.displayName,
          photoURL: 'https://pbs.twimg.com/profile_images/800034575476924416/aHVU_VqF_400x400.jpg'
        }).then(() => {
          this.firedata.child(this.afAuth.auth.currentUser.uid).set({
            uid: this.afAuth.auth.currentUser.uid,
            displayName: this.afAuth.auth.currentUser.displayName,
            email: this.afAuth.auth.currentUser.email,
            photoURL: 'https://pbs.twimg.com/profile_images/800034575476924416/aHVU_VqF_400x400.jpg',
            skippedImg: false,
            regMethod: "email"
          }).then(() => {
            resolve({ success: true });
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  /* addFbUser() {
    let provider = new firebase.auth.FacebookAuthProvider();

    var promise = new Promise((resolve, reject) => {
      firebase.auth().signInWithRedirect(provider).then(() => {
        firebase.auth().getRedirectResult().then((result) => {
          this.afAuth.auth.currentUser.updateProfile({
            displayName: firebase.auth().currentUser.displayName ,
            photoURL: 
          })
        }).catch(function (error) {
          alert(JSON.stringify(error));
        });
      });
    })
  } */

  /* updateimage(imageurl) {
    var promise = new Promise((resolve, reject) => {
      this.afireauth.auth.currentUser.updateProfile({
        displayName: this.afireauth.auth.currentUser.displayName,
        photoURL: imageurl
      }).then(() => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid).update({
          uid: firebase.auth().currentUser.uid,
          displayName: this.afireauth.auth.currentUser.displayName,
          email: this.afAuth.auth.currentUser.email,
          photoURL: imageurl
        }).then(() => {
          resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  } */

  updateimage(imageurl) {
    var promise = new Promise((resolve, reject) => {
      this.afAuth.auth.currentUser.updateProfile({
        displayName: this.afAuth.auth.currentUser.displayName,
        photoURL: imageurl
      }).then(() => {
        firebase.database().ref('/users/' + firebase.auth().currentUser.uid).update({
          displayName: this.afAuth.auth.currentUser.displayName,
          email: this.afAuth.auth.currentUser.email,
          photoURL: imageurl,
          skippedImg: true,
          uid: this.afAuth.auth.currentUser.uid
        }).then(() => {
          resolve({ success: true })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  updatedisplayname(newname) {
    this.checkImgStatus();
    var promise = new Promise((resolve, reject) => {
      this.afireauth.auth.currentUser.updateProfile({
        displayName: newname,
        photoURL: this.afireauth.auth.currentUser.photoURL
      }).then(() => {
        this.firedata.child(firebase.auth().currentUser.uid).update({
          displayName: newname,
          email: this.afAuth.auth.currentUser.email,
          photoURL: this.afireauth.auth.currentUser.photoURL,
          skippedImg: this.imgStatus,
          uid: this.afireauth.auth.currentUser.uid,
        }).then(() => {
          resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  authCheck() {
    /* kasutaja (edasi: user) staatus (kas on sisse logitud või mitte)
    /  kasutaja logitakse kohe peale registreerimist sisse. ilma selleta oleks väga raske kontrollida, 
    /  kas kasutajal on email aktiveeritud, nimi olemas jms. */
    firebase.auth().onAuthStateChanged((user) => {
      // kui user on sisse logitud
      if (user) {
        // kui user ei ole null; user kindlalt eksisteerib, koos andmetega
        if (user != null) {
          // võtame kasutajalt auth andmebaasist info ja paneme muutujatesse
          var emailVerified = user.emailVerified;
          if (!emailVerified) {
            return true;
          }
        }
      }
    });
  }

  /* testing() {
    var curUserID = this.afAuth.auth.currentUser.uid;
    this.firereq.orderByChild(curUserID).once('value', (snapshots) => {
      let somekeys = snapshots.val();
      let keyarr = [];
      for(var key in somekeys) {
        keyarr.push(somekeys[key].sender);
      }



      
      console.log(keyarr);
    })
  } */

  displayfilteredusers() {
    var curUserID = this.afAuth.auth.currentUser.uid;
    var promise = new Promise((resolve, reject) => {
      this.firedata.orderByChild('uid').once('value', (snapshot) => {
        let userdata = snapshot.val();
        let temparr = [];
        let forbiddenarr = [];
        

        this.firefriends.child(curUserID).orderByChild('uid').once('value', (snapshot) => {
          let frienddata = snapshot.val();
          let friendarr = [];
          for(var f in frienddata) {
            forbiddenarr.push(frienddata[f].uid);
          }

          /* for(var t in temparr) {
            for(var f in friendarr) {
              if(temparr[t].uid == friendarr[f]) {
                console.log("friend: "+t);
                temparr.splice(parseInt(f), 1);
              }
            }
          } */
        })

        

        this.firereq.child(curUserID).once('value', (snapshots) => {
          let somekeys = snapshots.val();
          let keyarr = [];
          for(var key in somekeys) {
            //keyarr.push(somekeys[key].sender);
            forbiddenarr.push(somekeys[key].sender);
          }
          /* for(var t in temparr) {
            for(var k in keyarr) {
              if(temparr[t].uid == keyarr[k]) {
                temparr.splice(parseInt(t), 1);
              }
            }
          } */
          /* console.log(keyarr); */
        })
        /* this.firedata.child(curUserID).orderByChild('uid').once('value', (snapshot) => {
          let tempreqs = snapshot.val();
          for(var req in tempreqs) {
            let tempremoved = temparr.indexOf[req];
            temparr.splice(tempremoved);
          }
        }) */
        for (var key in userdata) {
          if(key != curUserID && !this.isInArray(key, forbiddenarr) ) {
            temparr.push(userdata[key]);
          }
        }
        resolve(temparr);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  isInArray(value, array) {
    return array.indexOf(value) > -1;
  }

  getallusers() {
    var curUserID = this.afAuth.auth.currentUser.uid;
    var promise = new Promise((resolve, reject) => {
      this.firedata.orderByChild('uid').once('value', (snapshot) => {
        let userdata = snapshot.val();
        let temparr = [];
        for (var key in userdata) {
          if(key != curUserID) {
            temparr.push(userdata[key]);
          }
        }
        /* this.firedata.child(curUserID).orderByChild('uid').once('value', (snapshot) => {
          let tempreqs = snapshot.val();
          for(var req in tempreqs) {
            let tempremoved = temparr.indexOf[req];
            temparr.splice(tempremoved);
          }
        }) */
        resolve(temparr);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  imgCheck(): boolean{
    /* kui kasutaja ei ole oma profiilipilti sätestanud ning ei ole ise profiilipildi
    /  sätestamist vahele jätnud (nt: kui programm on kogemata kinni läinud/lõpetanud töötamise vms)
    /  , siis saadame ta lehele, kus ta seda teha saab */
    if (!this.checkImgStatus) {
      return true;
    }
  }

  getuserdetails() {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(firebase.auth().currentUser.uid).once('value', (snapshot) => {
        resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }


  checkImgStatus(){
    this.getuserdetails().then((res: any) => {
      this.imgStatus = res.skippedImg;
    });
    return this.imgStatus;
  }

  passwordReset(email) {
    var promise = new Promise((resolve, reject) => {
      firebase.auth().sendPasswordResetEmail(email).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

}
